$env.config.show_banner = false
$env.config.display_errors.exit_code = true
$env.config.table.mode = "light"
$env.config.table.trim.methodology = "wrapping"
$env.config.footer_mode = "auto"
$env.config.use_kitty_protocol = true
$env.config.edit_mode = "vi"
$env.config.datetime_format.normal = "%F %T"
$env.config.history.file_format = "sqlite"

$env.config.keybindings = [
  { name: insert-last-word
    modifier: alt
    keycode: char_.
    mode: vi_insert
    event: [
      { edit: InsertString, value: "!$" }
      { send: Enter }
    ]
  }
]

def "fuzzy directory" [query: string = ""] {
  fzf --walker=dir,hidden -q $query
}

def "fuzzy file" [query: string = ""] {
  fzf --walker=file,hidden -q $query
}

def "fuzzy edit" [query: string = ""] {
  fuzzy file $query | nvim $in
}

def --env "fuzzy cd" [query: string = ""] {
  fuzzy directory $query | cd $in
}

def --env up [count:int = 1] {
  use std
  "." | std repeat ($count + 1) | str join | cd $in
}

alias d = fuzzy directory
alias f = fuzzy file
alias ef = fuzzy edit
alias cdd = fuzzy cd

alias ll = ls -l
alias cp = cp -iv
alias mv = mv -iv
alias rm = rm -v
alias e = nvim
alias o = start
alias ext = aunpack
alias u = systemctl --user --no-block
alias man = batman
alias trash = rm --trash
alias wsudo = sudo --preserve-env=XDG_RUNTIME_DIR,WAYLAND_DISPLAY

stty -ixon # disable ctrl-s
source ~/.config/nushell/catppuccin-macchiato.nu
source ~/.config/nushell/zoxide.nu
source ~/.config/nushell/carapace.nu
source ~/.config/nushell/starship.nu

use '/home/tobias/.config/broot/launcher/nushell/br' *
