use path
use str


# aliases
fn ls  {|@a| eza -F $@a }
fn la  {|@a| ls -a $@a }
fn ll  {|@a| ls -lgh --color-scale size --git $@a }
fn lla {|@a| ll -a $@a }
fn cp  {|@a| e:cp -iv $@a }
fn mv  {|@a| e:mv -iv $@a }
fn rm  {|@a| e:rm -v $@a }
fn e   {|@a| nvim $@a }
fn ext {|@a| aunpack $@a }
fn u   {|@a| systemctl --user --no-block $@a }
fn a   {|@a| aria2c $@a }
fn t   {|@a| task $@a }
fn tk  {|@a| nvim -c 'Telekasten panel' }
fn man {|@a| batman $@a }
fn cdr {|@a| cd (git rev-parse --show-toplevel) }
fn o {|@a| gio open $@a }
fn trash {|@a| gio trash $@a }
fn wsudo {|@a| sudo --preserve-env=XDG_RUNTIME_DIR,WAYLAND_DISPLAY $@a }


fn br {|@a|
  var tmp = (mktemp)
  try {
    broot --outcmd $tmp $@a
  } catch e {
    nop
  } else {
    eval (slurp < $tmp)
  } finally {
    (external rm) -f $tmp
  }
}

fn up {|@arg|
  var count = 1
  if (not-eq $arg []) { set count = $arg[0] }
  cd (str:join "/" [(repeat $count "..")])
}

# poor mans vcs
fn keep {|f|
  try {
    var latest = (order &reverse=$true [$f.*[number]] | take 1)
    cmp -s $f $latest
    echo $f "not modified" >&stderr
  } catch e {
    var ext = (path:ext $f)
    var bname = (str:trim-suffix (path:base $f) $ext)
    cp -va $f $bname"_"(date '+%s')$ext
  }
}

# create and return current clutter folder
fn cl {
  var parent = ~/clutter
  var week = (date '+%G/%V')
  mkdir -p $parent"/"$week
  ln -sfT $week $parent"/current"
  put $parent"/"$week
}

# replace symlink with target
fn unlink {|a|
  if (not (or (path:is-regular $a) (path:is-dir $a))) {
    var path = (path:eval-symlinks $a)
    rm $a
    cp -r $path $a
  }
}

# fuzzy file
fn f {|@query|
  if (eq $query []) { set query = "" }
  fzf --walker=file,hidden -q (str:join " " $query)
}

# fuzzy directory
fn d {|@query|
  if (eq $query []) { set query = "" }
  fzf --walker=dir,hidden -q $query
}

# fuzzy cd
fn cdd {|@query|
  cd (d $@query)
}

# fuzzy edit
fn ef {|@query|
  nvim (f $@query)
}


# keybindings
{
  var f = {|k f| set edit:insert:binding[$k] = $f }

  $f C-A $edit:move-dot-sol~
  $f C-E $edit:move-dot-eol~
  $f C-F $edit:move-dot-right~
  $f C-B $edit:move-dot-left~
  $f A-f $edit:move-dot-right-word~
  $f A-b $edit:move-dot-left-word~
  $f C+A-f $edit:move-dot-right-small-word~
  $f C+A-b $edit:move-dot-right-small-word~

  $f C-t $edit:transpose-rune~
  $f A-t $edit:transpose-word~
  $f C+A-t $edit:transpose-small-word~

  $f C-k $edit:kill-line-right~
  $f C-u $edit:kill-line-left~
  $f A-d $edit:kill-word-right~
  $f A-Backspace $edit:kill-word-left~
  $f C+A-d $edit:kill-small-word-right~
  $f C+A-Backspace $edit:kill-small-word-left~

  $f C-L { edit:clear }

  $f C-H $edit:history:fast-forward~
  $f A-n $edit:navigation:start~
  $f A-l $edit:location:start~
}


# hooks
# OSC-7 for spawning new terminal in same dir
set after-chdir = [{|_| printf "\e]7;file://%s%s\e\\" (hostnamectl hostname) $pwd }]


# prompt
eval (starship init elvish)


# completions
eval (carapace _carapace | slurp)
set edit:completion:matcher[''] = {|seed| edit:match-prefix $seed &ignore-case=$true }
set edit:completion:arg-completer[gopass] = {|@args|
  gopass list --flat
}


# environment
set value-out-indicator = ':> '
eval (dircolors -c ~/.dircolors | sed 's/setenv/set-env/')
set-env SUDO_PROMPT "[sudo] "$E:USER@(hostnamectl hostname)"'s passwd: "
stty -ixon # disable ctrl-s
set-env LESS "-i"

eval (zoxide init elvish | slurp)
