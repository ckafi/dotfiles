local opt = vim.opt

opt.completeopt:append("menuone") -- Use compl even when there is only one match
opt.completeopt:append("noselect") -- Don't select a match
opt.completeopt:append("noinsert") -- or insert any text

opt.expandtab = true -- Use appropriate number of spaces instead of a tab
opt.shiftwidth = 0 -- Number of spaces for each (auto)indent (0->ts)
opt.smartindent = true -- Use smart-indenting
opt.softtabstop = -1 -- Number of spaces a Tab counts for in insert (-1->sw)
opt.tabstop = 2 -- Number of spaces a Tab counts for in file

opt.fillchars = "vert:┃" -- Thicker VertSplit border
opt.fillchars:append("diff: ") -- No filler in diff

opt.formatoptions:append("n") -- Recognize numbered lists
opt.formatoptions:append("r") -- Inset comment leader after <Enter>
opt.formatoptions:remove("t") -- Don't automatically format text

opt.ignorecase = true -- Do case insensitive matching
opt.inccommand = "split" -- Show live substitutions in split
opt.lazyredraw = true -- Do not redraw the screen during macros
opt.linebreak = true -- (Soft)wrap long lines at line break

opt.listchars = "space:·" -- List mode character for space
opt.listchars:append("eol:⌐") -- ... end of line
opt.listchars:append("lead: ") -- don't show leading spaces

opt.mouse = "nv" -- Enable mouse in normal mode
opt.mousemodel = "popup" -- Don't extend selection on right click

opt.number = true -- Show line number
opt.scrolloff = 3 -- Number of lines always above/below cursor
opt.shortmess:append("c") -- don't give ins-completion-menu messages.
opt.shortmess:append("I") -- no intro message
opt.showbreak = "└ " -- Char to show at beginning of wrapped lines
opt.showmatch = true -- Show matching brackets.
opt.smartcase = true -- Do smart case matching
opt.splitbelow = true -- For split: new window below old one
opt.splitright = true -- For vsplit: new window right of old one
opt.switchbuf:append("useopen") -- Switch to buffer when already open
opt.termguicolors = true -- Enable 24-bit colors
opt.textwidth = 100 -- Max. length of line for auto-formatting
opt.title = true -- set window title
opt.undofile = true -- Save undo history
opt.visualbell = true -- Visual bell instead of beeping
opt.wildmode = "longest:full" -- Complete longest common string and start wildmenu
opt.wildmode:append("full") -- ... then go to next full match
opt.diffopt:append("algorithm:histogram") -- use histogram algorithm for diff view
opt.shada:append("r/mnt") -- don't load shada data for files under /mnt
opt.conceallevel = 2 -- conceal certain texts, like links in markdown

opt.grepprg = "rg --vimgrep --no-heading --smart-case"
opt.grepformat = "%f:%l:%c:%m"
