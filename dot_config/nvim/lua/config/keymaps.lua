local map = vim.keymap.set

vim.g.mapleader = " "

map("n", "<c-j>", "<c-^>")
map("t", "<esc><esc>", "<c-\\><c-n>", { desc = "Terminal escape" })
map("n", "<MiddleMouse>", "<LeftMouse>")

map("v", "<", "<gv", { desc = "Outdent selection" })
map("v", ">", ">gv", { desc = "Indent selection" })

-- Diagnostics
map("n", "grq", vim.diagnostic.setqflist, { desc = "Populate quickfix list with diagnostics" })
map("n", "grl", vim.diagnostic.setloclist, { desc = "Populate location list with diagnostics" })
map("n", "<c-w>q", "<cmd>copen<cr>", { desc = "Open quickfix window" })

map("n", "ä", "[", { remap = true })
map("n", "ü", "]", { remap = true })

-- Tags
map("n", "gü", "g<c-]>", { desc = "Go to tag" })
map("n", "<c-w>ü", "<c-w>g<c-]>", { desc = "Open new window for tag" })
map("n", "<c-w>ö", "<c-w>g}", { desc = "Preview tag" })
