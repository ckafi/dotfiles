local augroup = vim.api.nvim_create_augroup("CursorLineOnlyInActiveWindow", {})
vim.api.nvim_create_autocmd({ "VimEnter", "WinEnter", "BufWinEnter" }, {
  group = augroup,
  pattern = "*",
  callback = function(ev)
    vim.wo.cursorline = true
  end,
})
vim.api.nvim_create_autocmd({ "WinLeave" }, {
  group = augroup,
  pattern = "*",
  callback = function(ev)
    vim.wo.cursorline = false
  end,
})
