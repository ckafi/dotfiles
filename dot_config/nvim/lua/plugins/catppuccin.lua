MiniDeps.add({ source = "catppuccin/nvim", name = "catppuccin" })

require("catppuccin").setup({
  dim_inactive = { enabled = true },
})

vim.cmd.colorscheme("catppuccin-macchiato")
