MiniDeps.add("rlane/pounce.nvim")

require("pounce").setup({
  accept_keys = "ITERANHSUDF",
})

vim.keymap.set({ "n", "v" }, ",", require("pounce").pounce, { desc = "Pounce!" })
