MiniDeps.add({
  source = "akinsho/toggleterm.nvim",
})

require("toggleterm").setup({ shade_terminals = false })
