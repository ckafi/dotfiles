MiniDeps.add({
  source = "kevinhwang91/nvim-bqf",
  depends = { "junegunn/fzf" },
})

require("bqf").setup({
  preview = {
    auto_preview = false,
    winblend = 0,
  },
})
