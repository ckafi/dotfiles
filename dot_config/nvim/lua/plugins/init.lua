local function load(name)
  path = vim.fn.stdpath("config") .. "/lua/plugins/" .. name .. ".lua"
  dofile(path)
end

local function now(name)
  MiniDeps.now(function()
    load(name)
  end)
end

local function later(name)
  MiniDeps.later(function()
    load(name)
  end)
end

load("mini")

now("catppuccin")
now("treesitter")
now("lspconfig")

later("bqf")
later("pounce")
later("zk")
later("toggleterm")
