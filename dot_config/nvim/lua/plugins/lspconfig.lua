MiniDeps.add("neovim/nvim-lspconfig")

local map = vim.keymap.set

vim.api.nvim_create_autocmd("LspAttach", {
  group = vim.api.nvim_create_augroup("UserLspConfig", {}),
  callback = function(ev)
    map("i", "<c-s>", vim.lsp.buf.signature_help, { buffer = ev.buf, desc = "Show signature help" })
    map("n", "grn", vim.lsp.buf.rename, { buffer = ev.buf, desc = "Rename symbol" })
    map("n", "grf", vim.lsp.buf.format, { buffer = ev.buf, desc = "Format buffer" })
    map("n", "gra", vim.lsp.buf.code_action, { buffer = ev.buf, desc = "Select code action" })
    map("n", "grr", vim.lsp.buf.references, { buffer = ev.buf, desc = "List references in qf window" })
    map("n", "gri", vim.lsp.buf.implementation, { buffer = ev.buf, desc = "List implementations in qf window" })
    map("n", "grc", vim.lsp.buf.incoming_calls, { buffer = ev.buf, desc = "List calls in qf window" })
    map("n", "gO", vim.lsp.buf.document_symbol, { buffer = ev.buf, desc = "List document symbols in qf window" })
    local orig_util_open_floating_preview = vim.lsp.util.open_floating_preview
    function vim.lsp.util.open_floating_preview(contents, syntax, opts, ...)
      opts = opts or {}
      opts.border = opts.border or "single"
      return orig_util_open_floating_preview(contents, syntax, opts, ...)
    end
  end,
})

local config_servers = {
  "texlab",
  "pyright",
  "clangd",
  "rust_analyzer",
  "zls",
}

for _, lsp in ipairs(config_servers) do
  require("lspconfig")[lsp].setup({
    capabilities = vim.lsp.protocol.make_client_capabilities(),
  })
end
