MiniDeps.add("echasnovski/mini.nvim")

local now, later = MiniDeps.now, MiniDeps.later

later(function()
  require("mini.ai").setup()
end)

later(function()
  require("mini.align").setup()
end)

later(function()
  require("mini.basics").setup({
    options = {
      basic = false,
      extra_ui = false,
      win_borders = "default",
    },
  })
end)

later(function()
  require("mini.bracketed").setup()
end)

later(function()
  local miniclue = require("mini.clue")
  miniclue.setup({
    triggers = {
      { mode = "n", keys = "<Leader>" },
      { mode = "x", keys = "<Leader>" },
      { mode = "i", keys = "<C-x>" },
      { mode = "n", keys = "g" },
      { mode = "x", keys = "g" },
      { mode = "n", keys = "'" },
      { mode = "n", keys = "`" },
      { mode = "x", keys = "'" },
      { mode = "x", keys = "`" },
      { mode = "n", keys = '"' },
      { mode = "x", keys = '"' },
      { mode = "i", keys = "<C-r>" },
      { mode = "c", keys = "<C-r>" },
      { mode = "n", keys = "<C-w>" },
      { mode = "n", keys = "z" },
      { mode = "x", keys = "z" },
      { mode = "n", keys = "[" },
      { mode = "n", keys = "]" },
      { mode = "n", keys = [[\]] },
    },
    clues = {
      miniclue.gen_clues.builtin_completion(),
      miniclue.gen_clues.g(),
      miniclue.gen_clues.marks(),
      miniclue.gen_clues.registers(),
      miniclue.gen_clues.windows(),
      miniclue.gen_clues.z(),
    },
    window = {
      config = { width = "auto" },
    },
  })
end)

now(function()
  require("mini.completion").setup({
    lsp_completion = {
      process_items = require("mini.fuzzy").process_lsp_items,
    },
    window = {
      info = { border = "single" },
      signature = { border = "single" },
    },
  })
end)

later(function()
  require("mini.cursorword").setup()
  vim.api.nvim_set_hl(0, "MiniCursorwordCurrent", {})
end)

later(function()
  require("mini.diff").setup({ view = { style = "sign" } })
end)

now(function()
  require("mini.extra").setup()
end)

later(function()
  require("mini.icons").setup()
  MiniIcons.tweak_lsp_kind()
end)

later(function()
  require("mini.indentscope").setup({
    draw = {
      delay = 0,
      animation = require("mini.indentscope").gen_animation.none(),
    },
    symbol = "▎",
  })
  vim.api.nvim_set_hl(0, "MiniIndentscopeSymbol", { link = "Whitespace" })
end)

later(function()
  require("mini.misc").setup()
  vim.keymap.set("n", "<c-w><c-z>", require("mini.misc").zoom, { desc = "Zoom window" })
end)

later(function()
  require("mini.notify").setup()
  vim.notify = MiniNotify.make_notify()
end)

now(function()
  require("mini.pick").setup()
  vim.ui.select = MiniPick.ui_select
  local map = vim.keymap.set
  map("n", "<leader>pb", MiniPick.builtin.buffers, { desc = "Pick buffers" })
  map("n", "<leader>pf", MiniPick.builtin.files, { desc = "Pick files" })
  map("n", "<leader>pg", MiniPick.builtin.grep_live, { desc = "Live grep" })
  map("n", "<leader>pk", MiniExtra.pickers.keymaps, { desc = "Pick from keymaps" })
  map("n", "<leader>pm", MiniExtra.pickers.marks, { desc = "Pick from marks" })
  map("n", "<leader>pR", MiniPick.builtin.resume, { desc = "Resume last picker" })
  map("n", "<leader>ph", MiniPick.builtin.help, { desc = "Pick form help tags" })

  -- lists
  map("n", "<leader>pj", function()
    MiniExtra.pickers.list({ scope = "jump" })
  end, { desc = "Pick from jumplist" })
  map("n", "<leader>pl", function()
    MiniExtra.pickers.list({ scope = "location" })
  end, { desc = "Pick from location list" })
  map("n", "<leader>pq", function()
    MiniExtra.pickers.list({ scope = "quickfix" })
  end, { desc = "Pick from quickfix list" })

  -- language server
  map("n", "<leader>ps", function()
    MiniExtra.pickers.lsp({ scope = "document_symbol" })
  end, { desc = "Pick from document symbols" })
  map("n", "<leader>pS", function()
    MiniExtra.pickers.lsp({ scope = "workspace_symbol" })
  end, { desc = "Pick from workspace_symbols" })
  map("n", "<leader>pd", function()
    MiniExtra.pickers.lsp({ scope = "definition" })
  end, { desc = "Pick from definitions" })
  map("n", "<leader>pi", function()
    MiniExtra.pickers.lsp({ scope = "implementation" })
  end, { desc = "Pick from implementations" })
  map("n", "<leader>pr", function()
    MiniExtra.pickers.lsp({ scope = "references" })
  end, { desc = "Pick from references" })
  map("n", "<leader>ptd", function()
    MiniExtra.pickers.lsp({ scope = "type_definition" })
  end, { desc = "Pick from type definitions" })
end)

now(function()
  require("mini.statusline").setup({
    content = {
      active = function()
        local mode, mode_hl = MiniStatusline.section_mode({ trunc_width = 75 })
        local git = MiniStatusline.section_git({ trunc_width = 75 })
        local diagnostics = MiniStatusline.section_diagnostics({ trunc_width = 75 })
        local fileinfo = MiniStatusline.section_fileinfo({ trunc_width = 120 })
        return MiniStatusline.combine_groups({
          { hl = mode_hl, strings = { mode } },
          { hl = "MiniStatuslineDevinfo", strings = { git, diagnostics } },
          "%<", -- Mark general truncate point
          { hl = "MiniStatuslineFilename", strings = { "%f%m" } },
          "%=", -- End left alignment
          { hl = "MiniStatuslineFileinfo", strings = { fileinfo } },
        })
      end,
      inactive = function()
        return "%#MiniStatuslineInactive#%f%m"
      end,
    },
  })
end)

later(function()
  require("mini.splitjoin").setup()
end)

later(function()
  require("mini.surround").setup()
end)

later(function()
  require("mini.trailspace").setup()
  vim.api.nvim_set_hl(0, "MiniTrailspace", { link = "DiffDelete" })
end)
