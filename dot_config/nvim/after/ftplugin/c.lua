vim.api.nvim_set_keymap("n", "<C-s>", '<cmd>ClangdSwitchSourceHeader<cr>', {noremap = true, silent = true})
vim.opt.formatoptions:remove("t")
vim.o.commentstring = "//%s"
