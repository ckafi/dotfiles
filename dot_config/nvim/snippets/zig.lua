return {
  s(
    "alloc_gpa",
    t({
      "var gpa = std.heap.GeneralPurposeAllocator(.{}){};",
      "defer std.debug.assert(gpa.deinit() == .ok);",
      "const allocator = gpa.allocator();",
    })
  ),
  s(
    "alloc_arena",
    t({
      "var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);",
      "defer arena.deinit();",
      "const allocator = arena.allocator();",
    })
  ),
}
